package peterlavalle
import scala.jdk.CollectionConverters.*
import java.io.File
import scala.annotation.targetName
import scala.collection.mutable

trait CLI extends CLI.App {
	final def main(args: Array[String]): Unit = main.call(args)
}

object CLI {
	trait App {

		def main: cli

		extension (name: String)

			def env[V: Parse](value: => V): Arg[V] =
				new Arg[V](name, () => value, true)

			@targetName("arg")
			def -<[V: Parse](value: V): Arg[V] =
				new Arg[V](name, () => value, false)


		implicit object parseBoolean extends Parse[Boolean] {
			override def parse(text: String): Boolean = text.toBoolean
		}

		implicit object parseInt extends Parse[Int] {
			override def parse(text: String): Int = text.toInt
		}

		implicit object parseString extends Parse[String] {
			override def parse(text: String): String = text
		}

		implicit object parseFile extends Parse[File] {
			override def parse(text: String): File = new File(text)
		}

		implicit protected class cli private[CLI](val call: Array[String] => Unit)

		trait Parse[V] {
			def parse(text: String): V
		}

		protected class Arg[V: Parse] private[CLI](name: String, value: () => V, env: Boolean) extends Bind[V] {

			private implicit class extensionArrayString(args: Array[String]) {
				def opt(name: String): Option[String] =
					args
						.find((_: String).startsWith(name + "="))
						.map((_: String).drop((name + "=").length))
			}

			implicit class extensionF[O](f: V => O) {
				def cure: Array[String] => O =
					(args: Array[String]) =>
						f(
							args
								.opt(name)
								.orElse {
									System
										.getProperties.asScala.toList
										.find((_: (String, String))._1 == name)
										.map((_: (String, String))._2)
								}
								.orElse {
									if (!env)
										None
									else
										System
											.getenv().asScala.toList
											.find((_: (String, String))._1 == name)
											.map((_: (String, String))._2)
								}
								.map(implicitly[Parse[V]].parse)
								.getOrElse {
									value()
								}
						)
			}

			def flatMap(f: V => cli): cli = (args: Array[String]) => f.cure(args).call(args)

			def map(f: V => Unit): cli = f.cure

			def as[T](vt: V => T): Bind[T] =
				val base = this
				new Bind[T] :
					override def flatMap(t: T => cli): cli = base.flatMap(v => t(vt(v)))

					override def map(t: T => Unit): cli = base.map(v => t(vt(v)))

		}

		trait Bind[V] {

			def flatMap(f: V => cli): cli

			def map(f: V => Unit): cli
		}


		object Parse {

			abstract class A[V](f: String => V) extends Parse[V] {
				override def parse(text: String): V = f(text)
			}

		}

	}

}

