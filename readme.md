[![](https://jitpack.io/v/com.gitlab.peterlavalle/cli.svg)](https://jitpack.io/#com.gitlab.peterlavalle/cli)

```
libraryDependencies += "com.gitlab.peterlavalle" % "cli" % "-SNAPSHOT"
```

this is a superclass for "command line with args" type programs.

... "why" i wrote this escapes me, but, it remains useful ...

```scala
override def main: cli =
    for {
        test <- "test" -< false
        icon <- "icon" -< "https://secure.gravatar.com/avatar/a3d639e38dd5ee07bb6881a6e662eb14"

        host <- "host" -< "localhost"
        port <- "port" -< 3030
        hval <- "hval" -< 540

        user <- "user" -< "target/user/".file
        root <- "root" -< (System.getProperty("user.home") + "/mp4-video/").file
        todo <- "todo" -< (System.getProperty("user.home") + "/Downloads/todo").file

        logs <- ("logs" -< user / "logs.txt").as(Logs(_, test))
        pool <- ("pool" -< 4).as(Pool(_, logs))

    } yield {
      ...
    }
```

... neat, eh?
